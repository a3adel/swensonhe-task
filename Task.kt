package com.example.swnsonhe

fun main() {
    println(is_anagram("asd adel", "adel   asd"))
    println("FIB(5) ${fib_recursive_approach(6)}")
}

fun is_anagram(first_text: String, second_text: String): Boolean {
    if (first_text == second_text)
        return true

    var first_text_without_spaces = first_text.replace(" ", "").toLowerCase()
    var second_text_without_spaces = second_text.replace(" ", "").toLowerCase()
    if (first_text_without_spaces.length != second_text_without_spaces.length)
        return false
    else {
        for (c: Char in first_text_without_spaces.toCharArray())
            second_text_without_spaces = second_text_without_spaces.replaceFirst(c.toString(), "")
        return second_text_without_spaces.isEmpty()
    }
}

fun fib_iterative_approach(n: Int) :Int{
    var fib = 1
    when (n) {
        1 -> fib = 1
        2 -> fib = 1
    }
    var previous_fib = 1
    var before_previous_fib = 1
    for (i in 3..n) {
        fib = previous_fib + before_previous_fib
        before_previous_fib = previous_fib
        previous_fib = fib
    }
    return fib
}

fun fib_recursive_approach(n:Int):Int{
    when(n){
        1-> return 1
        2-> return 1
    }
    var fib = 1
    fib = fib_recursive_approach(n-1)+ fib_recursive_approach(n-2)
    return fib
}
