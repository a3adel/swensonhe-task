package com.example.currencyconverter.ui.main

import android.os.Bundle
import android.os.PersistableBundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.R
import com.example.currencyconverter.data.models.CurrencyRate
import com.example.currencyconverter.ui.base.BaseActivity
import com.example.currencyconverter.ui.utils.OnRecyclerViewItemClickListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class MainActivityView : BaseActivity(), MainActivityMVPView {
    @Inject
    lateinit var presenter: MainActivityPresenter<MainActivityMVPView>
    lateinit var adapter: CurrenciesAdapter
    var rates: ArrayList<CurrencyRate> = ArrayList()
    var currentRate = 1.0f
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityComponent.inject(this)
        presenter.onAttach(this)

        presenter.getCurrenciesExchangeRate()
        adapter = CurrenciesAdapter()
        adapter.setOnRecylerViewItemClickListener(object : OnRecyclerViewItemClickListener {
            override fun onItemClicked(position: Int) {
                currenciesRecyclerView.visibility = View.GONE
                getRateConstraintLayout.visibility = View.VISIBLE
                euroCurrencyEditText.setText("1")
                targetCurrencyTextView.text = rates?.get(position)?.name
                currentRate = rates?.get(position).rateValue
                presenter.convertCurrency(1.0, rates?.get(position).rateValue)
            }
        })
        currenciesRecyclerView.adapter = adapter
        currenciesRecyclerView.addItemDecoration(
            DividerItemDecoration(
                this,
                LinearLayoutManager.HORIZONTAL
            )
        )
        currenciesRecyclerView.layoutManager = LinearLayoutManager(this)
        euroCurrencyEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = 0.0
                try {
                    val text = s.toString()
                    value = text.toDouble()
                } catch (e: Exception) {
                    value = 0.0
                }
                presenter.convertCurrency(value, currentRate)
            }
        })

    }

    override fun returnRates(rates: List<CurrencyRate>) {
        this.rates?.clear()
        this.rates?.addAll(rates)
        adapter.currenciesList.clear()
        adapter.currenciesList.addAll(rates)
        adapter.notifyDataSetChanged()
    }

    override fun onError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onTargetCurrencyValueUpdated(value: Double) {
        targetCurrencyEditText.setText(value.toString())
    }

    override fun showProgressbar() {
        loadingProgressbar.visibility = View.VISIBLE
    }

    override fun hideProgressbar() {
        loadingProgressbar.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (getRateConstraintLayout.visibility == View.VISIBLE) {
            getRateConstraintLayout.visibility = View.GONE
            currenciesRecyclerView.visibility = View.VISIBLE
        } else
            super.onBackPressed()
    }
}