package com.example.currencyconverter.data

import com.example.currencyconverter.data.models.APIResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface APIEndpoints {
    @GET("/api/latest")
    fun getRates(): Deferred<APIResponse>

    class Creator {
        companion object {
            val BASE_URL = "http://data.fixer.io/"
            val API_KEY = "aaa9d12bf60c2587bb15639ead4ad0a8"
            fun newAPIService(): APIEndpoints {
                var httpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
                httpClientBuilder.addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val url = chain.request().url.newBuilder()
                            .addQueryParameter("access_key", API_KEY)
                            .build()
                        val request = chain.request()
                            .newBuilder()
                            .url(url)
                            .build()
                        return chain.proceed(request)
                    }
                })
                val logging = HttpLoggingInterceptor()
                logging.setLevel(HttpLoggingInterceptor.Level.BODY)

                val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClientBuilder.build())
                    .baseUrl(BASE_URL)
                    .build()
                return retrofit.create(APIEndpoints::class.java)
            }
        }
    }

}