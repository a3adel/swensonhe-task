package com.example.currencyconverter.data

import android.content.Context
import com.example.currencyconverter.data.models.APIResponse
import com.example.currencyconverter.di.ApplicationContext
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDataManager @Inject constructor(
    @ApplicationContext val context: Context,
    val apiEndpoints: APIEndpoints
) : DataManager {

    override suspend fun getRates(): APIResponse {
        val rates = apiEndpoints.getRates().await()
        return rates
    }


}