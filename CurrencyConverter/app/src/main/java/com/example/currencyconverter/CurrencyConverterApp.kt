package com.example.currencyconverter

import android.app.Application
import com.example.currencyconverter.data.DataManager
import com.example.currencyconverter.di.components.AppComponent
import com.example.currencyconverter.di.components.DaggerAppComponent
import com.example.currencyconverter.di.modules.AppModule
import javax.inject.Inject

class CurrencyConverterApp : Application() {
    lateinit var mApplicationComponent: AppComponent
    @Inject
    lateinit var mDataManager: DataManager
    override fun onCreate() {
        super.onCreate()
        mApplicationComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        mApplicationComponent.inject(this)
    }

    fun getComponent(): AppComponent {
        return mApplicationComponent
    }
}