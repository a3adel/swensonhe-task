package com.example.currencyconverter.ui.main

import com.example.currencyconverter.data.models.CurrencyRate
import com.example.currencyconverter.ui.base.MVPView

interface MainActivityMVPView : MVPView {
    fun returnRates(rates: List<CurrencyRate>)
    fun onError(message:String)

    fun onTargetCurrencyValueUpdated(value: Double)
    fun showProgressbar()
    fun hideProgressbar()

}