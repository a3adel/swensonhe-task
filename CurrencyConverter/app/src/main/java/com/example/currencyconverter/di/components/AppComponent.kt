package com.example.currencyconverter.di.components

import android.app.Application
import android.content.Context
import com.example.currencyconverter.CurrencyConverterApp
import com.example.currencyconverter.data.DataManager
import com.example.currencyconverter.di.ApplicationContext
import com.example.currencyconverter.di.modules.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app:CurrencyConverterApp)
    fun getDataManager():DataManager
    @ApplicationContext
    fun context(): Context
    fun application():Application
}