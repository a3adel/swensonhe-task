package com.example.currencyconverter.di.components

import com.example.currencyconverter.ui.main.MainActivityView
import com.example.currencyconverter.di.PerActivity
import com.example.currencyconverter.di.modules.ActivityModule
import dagger.Component

@PerActivity
@Component(dependencies = [AppComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(mainActivity: MainActivityView)
}