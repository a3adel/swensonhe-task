package com.example.currencyconverter.ui.main

import com.example.currencyconverter.di.PerActivity
import com.example.currencyconverter.ui.base.BasePresenter
import com.example.currencyconverter.ui.base.MVPPresenter
import com.example.currencyconverter.ui.base.MVPView
@PerActivity
interface MainActivityPresenter<V:MVPView>:MVPPresenter<V>{
    fun getCurrenciesExchangeRate()
    fun convertCurrency(value: Double, targetRate: Float)
}