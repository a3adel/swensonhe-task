package com.example.currencyconverter.di.modules

import android.app.Application
import android.content.Context
import com.example.currencyconverter.data.APIEndpoints
import com.example.currencyconverter.data.AppDataManager
import com.example.currencyconverter.data.DataManager
import com.example.currencyconverter.di.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val application: Application) {

    @Provides
    @ApplicationContext
    fun provideContext(): Context {
        return application
    }

    @Provides
    fun provideApplication(): Application {
        return application
    }

    @Provides
    @Singleton
    fun provideApiEndPoints():APIEndpoints{
        return APIEndpoints.Creator.newAPIService()
    }
    @Provides
    @Singleton
    fun provideDataManager(appDataManager: AppDataManager):DataManager{
        return appDataManager
    }


}