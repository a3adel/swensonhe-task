package com.example.currencyconverter.data.models

data class CurrencyRate(val name: String, val rateValue: Float, val logo: String?) {
}