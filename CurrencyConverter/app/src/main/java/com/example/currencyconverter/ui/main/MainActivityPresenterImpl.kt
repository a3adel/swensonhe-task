package com.example.currencyconverter.ui.main

import com.example.currencyconverter.data.DataManager
import com.example.currencyconverter.data.models.CurrencyRate
import com.example.currencyconverter.ui.base.BasePresenter
import kotlinx.coroutines.*
import javax.inject.Inject

class MainActivityPresenterImpl<V : MainActivityMVPView> @Inject constructor(mDataManager: DataManager) :
    BasePresenter<V>(mDataManager), MainActivityPresenter<V> {
    override fun getCurrenciesExchangeRate() {
        getMVPView()?.showProgressbar()
        if (getMVPView()?.isConnectedToNetwork()!!) {
            val job = CoroutineScope(Dispatchers.IO).launch {
                val apiResponse = mDataManager.getRates()
                val ratesJsonObject = apiResponse.rate
                val currencyRates: ArrayList<CurrencyRate> = ArrayList()
                ratesJsonObject?.asJsonObject?.keySet()?.forEach { key ->
                    val flag_icon = "flag_${key.toLowerCase()}"
                    currencyRates.add(
                        CurrencyRate(
                            key,
                            ratesJsonObject.asJsonObject.get(key).asFloat,
                            flag_icon
                        )
                    )
                }
                withContext(Dispatchers.Main) {
                    getMVPView()?.returnRates(currencyRates)

                }
            }
        } else
            getMVPView()?.onError("Please connect to network and try again")

        getMVPView()?.hideProgressbar()
    }

    override fun convertCurrency(value: Double, targetRate: Float) {
        val targetValue = value * targetRate;
        getMVPView()?.onTargetCurrencyValueUpdated(targetValue)
    }
}