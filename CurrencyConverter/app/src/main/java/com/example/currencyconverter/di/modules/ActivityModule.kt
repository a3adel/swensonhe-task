package com.example.currencyconverter.di.modules

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.example.currencyconverter.di.ActivityContext
import com.example.currencyconverter.di.PerActivity
import com.example.currencyconverter.ui.main.MainActivityMVPView
import com.example.currencyconverter.ui.main.MainActivityPresenter
import com.example.currencyconverter.ui.main.MainActivityPresenterImpl
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(val activity: AppCompatActivity) {
    @Provides
    @ActivityContext
    fun provideContext(): Context = activity

    @Provides
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    @PerActivity
    fun provideMainPresenter(mainActivityPresenterImpl: MainActivityPresenterImpl<MainActivityMVPView>):
            MainActivityPresenter<MainActivityMVPView> {
        return mainActivityPresenterImpl
    }
}