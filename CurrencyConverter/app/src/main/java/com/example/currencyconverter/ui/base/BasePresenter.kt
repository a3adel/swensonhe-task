package com.example.currencyconverter.ui.base

import com.example.currencyconverter.data.DataManager
import java.lang.RuntimeException
import javax.inject.Inject

open class BasePresenter<V : MVPView> @Inject constructor(val mDataManager: DataManager) :
    MVPPresenter<V> {
    private var mMVPView: V? = null
    override fun onAttach(mvpView: V) {
        mMVPView = mvpView
    }

    override fun onDetach() {
        mMVPView = null
    }

    fun getMVPView(): V? = mMVPView

    fun isViewAttached(): Boolean = mMVPView != null

    fun getDataManager(): DataManager = mDataManager
    fun checkViewAttached(){
        if(!isViewAttached())throw MvpViewNotAttachedException()
    }
    class MvpViewNotAttachedException : RuntimeException(
        "Please call Presenter.onAttach(MvpView) before" +
                " requesting data to the Presenter"
    )
}