package com.example.currencyconverter.data.models

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import org.json.JSONObject

data class APIResponse(
    @SerializedName("success") var success: Boolean?=null,
    @SerializedName("timestamp") var timeStamp: Long?=null,
    @SerializedName("base") var baseCurrency: String?=null,
    @SerializedName("date") var date: String?=null,
    @SerializedName("rates") var rate: JsonElement?=null
)
