package com.example.currencyconverter.ui.base

interface MVPPresenter<V : MVPView?> {
    fun onAttach(mvpView: V)
    fun onDetach()

}