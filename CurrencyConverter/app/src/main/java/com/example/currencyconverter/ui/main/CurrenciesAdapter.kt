package com.example.currencyconverter.ui.main

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.R
import com.example.currencyconverter.data.models.CurrencyRate
import com.example.currencyconverter.ui.utils.OnRecyclerViewItemClickListener
import kotlinx.android.synthetic.main.item_currency.view.*
import java.lang.Exception

class CurrenciesAdapter : RecyclerView.Adapter<CurrenciesAdapter.CurrencyViewHolder>() {

    class CurrencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    private var onRecyclerViewItemClickListener: OnRecyclerViewItemClickListener? = null
    val currenciesList: ArrayList<CurrencyRate>
    lateinit var resources: Resources

    init {
        currenciesList = ArrayList()
    }

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
        context = parent.context
        resources = context.resources
        return CurrencyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return currenciesList.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.itemView.currencyNameTextView.text = currenciesList.get(position).name
        holder.itemView.exchangeRateTextView.text =
            currenciesList.get(position).rateValue.toString()
        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                onRecyclerViewItemClickListener?.onItemClicked(position)
            }
        })
        val resourceId = resources.getIdentifier(
            currenciesList.get(position).logo,
            "drawable",
            context.packageName
        )
        try {
            holder.itemView.flagIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    resourceId
                )
            )
        } catch (e: Exception) {
        }
    }

    fun setOnRecylerViewItemClickListener(onRecyclerViewItemClickListener: OnRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener
    }

}