package com.example.currencyconverter.ui.base

interface MVPView {
    fun isConnectedToNetwork():Boolean
}