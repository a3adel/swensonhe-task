package com.example.currencyconverter.data

import com.example.currencyconverter.data.models.APIResponse

interface DataManager {
    suspend fun getRates():APIResponse
}