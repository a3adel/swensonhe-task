package com.example.currencyconverter.ui.base

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.example.currencyconverter.CurrencyConverterApp
import com.example.currencyconverter.di.components.ActivityComponent
import com.example.currencyconverter.di.components.DaggerActivityComponent
import com.example.currencyconverter.di.modules.ActivityModule
import com.example.currencyconverter.ui.utils.NetworkUtils

abstract class BaseActivity : AppCompatActivity(), MVPView {
    lateinit var activityComponent: ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent =
            DaggerActivityComponent.builder().activityModule(ActivityModule(this))
                .appComponent((application as CurrencyConverterApp).getComponent()).build()
    }

    override fun isConnectedToNetwork(): Boolean {
        return NetworkUtils.isNetworkConnected(this)
    }

    fun getComponent(): ActivityComponent = activityComponent
}